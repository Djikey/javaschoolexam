package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO : Implement your solution here
        //Check amount of numbers
        if (inputNumbers.size() == 1) {
            throw new CannotBuildPyramidException();
        }
        long validAmount = 1;
        int rows;
        int columns;
        for (int i = 2; ; i++) {
            validAmount += i;
            if (inputNumbers.size() > validAmount) { //for quick verification
            } else if (inputNumbers.size() == validAmount) {
                rows = i;
                columns = 2*i - 1;
                break;
            } else if (inputNumbers.size() < validAmount) {
                throw new CannotBuildPyramidException();
            }
        }
        List<Integer> input = inputNumbers;
        //There may be nulls inside
        try {
            Collections.sort(input);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        //Array filling
        int[][] result = new int[rows][columns];
        int likeIterator = 0;
        for (int i = 0; i < rows; i++) {
            boolean digit = true;
            int zeros = (columns - ((i + 1)*2 - 1))/2;
            for (int j = 0; j < zeros; j++) {
                result[i][j] = 0;
            }
            for (int j = zeros; j < columns - zeros; j++) {
                if (digit) {
                    result[i][j] = input.get(likeIterator++);
                    digit = !digit;
                } else {
                    result[i][j] = 0;
                    digit = !digit;
                }
            }
            for (int j = columns - zeros; j < columns; j++) {
                result[i][j] = 0;
            }
        }
        return result;
    }


}
