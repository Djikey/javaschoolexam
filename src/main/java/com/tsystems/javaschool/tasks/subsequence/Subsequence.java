package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        List copyOfY = y;
        Iterator xIter = x.iterator();
        Iterator yIter = copyOfY.iterator();
        while (xIter.hasNext()) {
            Object objX = xIter.next();
            while (yIter.hasNext()) {
                Object objY = yIter.next();
                if (!objX.equals(objY)) {
                    yIter.remove();
                } else {
                    break;
                }
            }
        }
        return copyOfY.containsAll(x);
    }
}
