package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private String doubleDigit = "-?[0-9]+(\\.[0-9]+)?";
    private Pattern intDigit = Pattern.compile("-?[0-9]+(\\.0)?");
    private Pattern multiplication = Pattern.compile(doubleDigit + "\\*" + doubleDigit);
    private Pattern division = Pattern.compile(doubleDigit + "/" + doubleDigit);
    private Pattern addition = Pattern.compile(doubleDigit + "\\+" + doubleDigit);
    private Pattern subtraction = Pattern.compile(doubleDigit + "-" + doubleDigit);
    private Pattern anyMultiOfDiv = Pattern.compile(multiplication.pattern() + "|" + division.pattern());
    private Pattern anyAddOfSub = Pattern.compile(addition.pattern() + "|" + subtraction.pattern());
    private Pattern parentheses = Pattern.compile("\\(.[^()]*\\)");
    private String doubleMinus = "--";
    private String doublePlus = "\\+\\+";
    private String plusMinus = "\\+-|-\\+";
    private Pattern trash = Pattern.compile("[+*/\\-]+?[+*/\\-]+?|\\)\\(|\\(\\)");

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        //Primary validation
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        Matcher matcher = trash.matcher(statement);
        if (matcher.find()) {
            return null;
        }
        //Calculation
        String result = calculate(statement);
        //Is it possible to parse? + Rounding
        double dResult;
        try {
            dResult = Math.round(Double.parseDouble(result) * 10000.0) / 10000.0;
        } catch (NumberFormatException e) {
            return null;
        } catch (NullPointerException e) {
            return null;
        }
        //What about integer?
        int iResult;
        String dString = String.valueOf(dResult);
        matcher = intDigit.matcher(dString);
        if (matcher.matches()) {
            iResult = Integer.parseInt(dString.substring(0, dString.length() - 2));
            return String.valueOf(iResult);
        }
        return result;
    }

    private String calculate (String statement) {
        String input = statement;
        Matcher matcher;

        try {
            //Remove leading and trailing parentheses
            matcher = parentheses.matcher(input);
            if (matcher.matches()) {
                input = input.substring(1, input.length() - 1);
            }
            //Search parentheses
            while (matcher.find()) {
                input = input.replace(matcher.group(), calculate(matcher.group()));
                input = mergeSign(input);
                matcher.reset(input);
            }
            //Try to multiply
            matcher = multiplication.matcher(input);
            if (matcher.matches()) {
                input = doMultiplication(input);
            }
            //Try to divide
            matcher = division.matcher(input);
            if (matcher.matches()) {
                input = doDivision(input);
            }
            //Search multiplication or division
            matcher = anyMultiOfDiv.matcher(input);
            while (matcher.find()) {
                input = input.replace(matcher.group(), calculate(matcher.group()));
                input = mergeSign(input);
                matcher.reset(input);
            }
            //Try to addition
            matcher = addition.matcher(input);
            if (matcher.matches()) {
                input = doAddition(input);
            }
            //Try to subtraction
            matcher = subtraction.matcher(input);
            if (matcher.matches()) {
                input = doSubtraction(input);
            }
            //Search addition or subtraction
            matcher = anyAddOfSub.matcher(input);
            while (matcher.find()) {
                input = input.replace(matcher.group(), calculate(matcher.group()));
                input = mergeSign(input);
                matcher.reset(input);
            }
        } catch (NullPointerException e) {
            return null;
        }
        return input;
    }

    private String doMultiplication (String statement) {
        String input = statement;
        double digit1 = Double.parseDouble(input.split("\\*")[0]);
        double digit2 = Double.parseDouble(input.split("\\*")[1]);
        if (digit1 < 0 && digit2 < 2) {
            return "+" + String.valueOf(digit1 * digit2);
        } else {
            return String.valueOf(digit1 * digit2);
        }
    }

    private String doDivision (String statement) {
        String input = statement;
        double digit1 = Double.parseDouble(input.split("/")[0]);
        double digit2 = Double.parseDouble(input.split("/")[1]);
        if (digit1 < 0 && digit2 < 0) {
            return "+" + String.valueOf(digit1 / digit2);
        } else if (digit2 != 0){
            return String.valueOf(digit1 / digit2);
        } else {
            return null;
        }
    }

    private String doAddition (String statement) {
        String input = statement;
        double digit1 = Double.parseDouble(input.split("\\+")[0]);
        double digit2 = Double.parseDouble(input.split("\\+")[1]);
        return String.valueOf(digit1 + digit2);
    }

    private String doSubtraction (String statement) {
        String input = statement;
        double digit1 = Double.parseDouble(input.split("-")[0]);
        double digit2 = Double.parseDouble(input.split("-")[1]);
        return String.valueOf(digit1 - digit2);
    }

    private String mergeSign (String statement) {
        //Just like in primary school
        String input = statement.replace(doubleMinus, "+");
        input = input.replace(doublePlus, "+");
        input = input.replace(plusMinus, "-");
        return input;
    }
}
